.. _usb:

Installing machines with a USB installer
========================================

Installing Debian manually on machines and running ansible by hand can be
tedious and error-prone.

Although :ref:`our advanced usage documentation <advanced\_usage>` covers
imaging via PXE, you might find that for a small number of machines, using a
pre-built USB installer works best. It's also useful if you want to bootstrap
the PXE server.

Like the PXE server, this installer first installs a preseeded image and then
runs ansible when the machine first boots.

The ``usbinst`` directory contains the required scripts to make such a USB
installer and to test it properly.

``mk_usb_installer.sh``
-----------------------

This is the main script to build the USB installer. It makes a USB installer
with a preseed configuration and runs ansible. It currently supports Debian and
Ubuntu.

The script is not self contained and needs bits & pieces from the ansible
repository and from the internet to work properly. It takes for granted that you
already have a working ansible setup.

Disclaimer: it uses ``sudo`` to write to the USB key, so you may be prompted for
a ``sudo`` password.

Here is a summary of what it does:

1. Generates a ``late_command.cfg`` file in ``roles/tftp-server/files/scripts``
   for ansible to configure the ansible ran in ``late_command.sh``
2. Downloads a installation image (Debian or Ubuntu)
3. Sets kernel parameters to preseed the installer
4. Formats and mounts the USB key
5. Installs the installation image on the USB key
6. Unmounts the USB key
7. Starts a simple web server on port 8007 using the :ref:`httpserver` script
   to serve additional content from ``roles/tftp-server/files/``

The Debian Installer will fetch preseed configuration and `late_command scripts`
from ``roles/tftp-server/files/``, so the machine you are using to build the USB
installer needs to be available on the same network as the target machine,
until d-i is done.

Usage
^^^^^

You will need to install these dependencies to be able to run the script
properly::

      $ sudo apt install curl git pmount dcfldd syslinux-efi syslinux-common

The script has two parameters:

``usb-device``: path to the usb device (for example: ``/dev/sdb``).

``config-filename``: path to the configuration file containing the settings to build the machine and run ansible.

You can call it this way::

    $ ./mk_usb_installer.sh usb-device config-filename

Configuration File
^^^^^^^^^^^^^^^^^^

Recommended parameters
""""""""""""""""""""""

The following parameters should most of the time be explicitly set in your
``config-filename`` file:

``hostname``: (default: voctotest). Hostname of the target machine. If your DHCP server attributes a hostname to the machine, this value will be overwritten by it.

``playbook_repo``: (default: https://salsa.debian.org/debconf-video-team/ansible). The repository containing the playbooks for setting up the target machine.

``inventory_repo``: (default: blank). The repository containing the inventory/hosts files to use. If it is blank, the inventory in the playbook repository is used.

``timezone``: (default: Europe/Brussels). Timezone string respecting tzdata format.

``username``: (default: videoteam). Username of the main user.

``user_password_crypted``: (default: changeme). Crypted password using ``mkpasswd -m sha-256``.

Optional parameters
""""""""""""""""""""

These other parameters can be useful, but their defaults are more sane and can be
left as is:

``arch``: (default: amd64). Architecture of the target machine.

``bootdev``: (default: /dev/sda). The device on the target machine to install the bootloader on.

``disk``: (default: /dev/sda). The device on the target machine to install onto.

``debian_ver``: (default: 9.7.0). Debian version number.

``suite``: (default: stretch). Distribution suite to use.

``booting_loc``: (default: http://deb.debian.org/debian/dists/${suite}/main/installer-${arch}/current/images)  Location of the boot images with the ``SA256SUMS`` and ``hd-media/boot.img.gz`` boot image files.

``iso``: (default: debian-${debian_ver}-${arch}-netinst.iso) Name of the ISO file

``iso_loc``: (default: https://cdimage.debian.org/debian-cd/current/${arch}/iso-cd) URL to the directory containing the ISO file.

``playbook_branch``: (default: master). The git branch in the playbook repository to use.

``inventory_repo_dir``: (default: ${playbook_repo}/inventory). The path to the inventory repository to use.

``load_firmware``: (default: false). Boolean. When true, d-i will prompt the user to provide non-free firmware blobs.

``vault_pw``: (default: blank). The password for ansible vault encoded in base64.

``preseed``: (default: url=$(hostname):8007). The location of the http server with additional preseed configuration and additional files. This is the machine the ``http_server.sh`` script is running on, or a PXE server configured by the tftp-server role. Specify a ``hostname:port``.

``more_appends``: (default: blank). Additional configuration to be appended to the kernel command line.

Step by step
^^^^^^^^^^^^

The script needs to be run in the ``usbinit`` directory::

      $ cd usbinst

You can then create a new ``config-filename`` to set preseed values::

      $ cp configs/blank.cfg configs/mybox.cfg

Once that is done, you can change the values listed before that seem important
to you. Finally, you can run the installer and use the key to image machines::

      $ ./mk_usb_installer.sh sdb configs/mybox.cfg

.. _httpserver:

``http_server.sh``
------------------

This script runs an HTTP server so that the USB installer can pull additional
configuration files. The root directory being served is
``roles/tftp-server/files``.

``test_pxe.sh``
---------------

This script tests the PXE booting environment by booting it in a QEMU x86_64
system. You will need to install those dependencies to run it properly::

    $ sudo apt install qemu brctl

You can then run the script this way::

    $ ./test_pxe.sh

``test_thumb.sh``
-----------------

This script tests the USB stick by booting it in a QEMU x86_64 system. You will
need to install this dependency to run it properly::

    $ sudo apt install qemu qemu-utils qemu-system ovmf

It only takes one argument - path to the USB device, for example ``/dev/sdb``::

    $ ./test_thumb.sh /dev/sdb
